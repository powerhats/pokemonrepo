﻿using Pokemon.Models;
using Pokemon.Repositories.Interfaces;
using Pokemon.Services.Interfaces;
using System;
using System.Threading.Tasks;
using Xunit;
using Moq;
using Pokemon.Services;
using Microsoft.AspNetCore.Mvc;

namespace PokemonTest.Services
{
    public class PokemonServiceTest
    {
        private readonly Mock<IPokemonRepository> _mockpokemonrepo;
        private readonly Mock<ITranslationRepository> _mocktranslationrepo;
        private readonly PokemonService _service;

        public PokemonServiceTest()
        {
            _mockpokemonrepo = new Mock<IPokemonRepository>();
            _mocktranslationrepo = new Mock<ITranslationRepository>();
            _service = new PokemonService(_mockpokemonrepo.Object, _mocktranslationrepo.Object);
        }

        [Fact]
        public async Task GetPokemonTranslatedInfo_IfHabitatIsCave_CallYodaTranslator()
        {
            string testurl = null;

            _mockpokemonrepo.Setup(rep => rep.GetPokemonFullInfo(It.Is<string>(p => p == "hjkjhkhk"))).ReturnsAsync(new PokemonResource { name = "hjkjhkhk", habitat = "cave", description = "wqewqewqe" });

            _mocktranslationrepo.Setup(rep => rep.Translator(It.IsAny<string>())).Callback<string>(x => testurl = x).ReturnsAsync(new TranslationResource { contents = new Contents { }, success = new Success { } });
            
            var result = await _service.GetPokemonTranslatedInfo("hjkjhkhk");

            Assert.Contains("yoda", testurl);
        }

        [Fact]
        public async Task GetPokemonTranslatedInfo_IfIsLegendary_CallYodaTranslator()
        {
            string testurl = null;

            _mockpokemonrepo.Setup(rep => rep.GetPokemonFullInfo(It.Is<string>(p => p == "hjkjhkhk"))).ReturnsAsync(new PokemonResource { name = "hjkjhkhk", habitat = "wrrwe", description = "wqewqewqe", isLegendary = true });

            _mocktranslationrepo.Setup(rep => rep.Translator(It.IsAny<string>())).Callback<string>(x => testurl = x).ReturnsAsync(new TranslationResource { contents = new Contents { }, success = new Success { } });

            var result = await _service.GetPokemonTranslatedInfo("hjkjhkhk");

            Assert.Contains("yoda", testurl);
        }

        [Fact]
        public async Task GetPokemonTranslatedInfo_IfIsNotLegendaryAndHabitatIsNotCave_CallShakespeareTranslator()
        {
            string testurl = null;

            _mockpokemonrepo.Setup(rep => rep.GetPokemonFullInfo(It.Is<string>(p => p == "hjkjhkhk"))).ReturnsAsync(new PokemonResource { name = "hjkjhkhk", habitat = "wrrwe", description = "wqewqewqe", isLegendary = false });

            _mocktranslationrepo.Setup(rep => rep.Translator(It.IsAny<string>())).Callback<string>(x => testurl = x).ReturnsAsync(new TranslationResource { contents = new Contents { }, success = new Success { } });

            var result = await _service.GetPokemonTranslatedInfo("hjkjhkhk");

            Assert.Contains("shakespeare", testurl);
        }

        [Fact]
        public async Task GetPokemonTranslatedInfo_IfTranslated_ChangeDescription()
        {
            _mockpokemonrepo.Setup(rep => rep.GetPokemonFullInfo(It.Is<string>(p => p == "hjkjhkhk"))).ReturnsAsync(new PokemonResource { name = "hjkjhkhk", habitat = "wrrwe", description = "wqewqewqe", isLegendary = true });

            _mocktranslationrepo.Setup(rep => rep.Translator(It.IsAny<string>())).ReturnsAsync(new TranslationResource { contents = new Contents { translated = "this is the translated text" }, success = new Success { total = 1 } });

            var result = await _service.GetPokemonTranslatedInfo("hjkjhkhk");

            Assert.Equal("this is the translated text", result.description);
        }
    }
}
