﻿using Microsoft.AspNetCore.Mvc;
using Pokemon.Models;
using Pokemon.Services.Interfaces;
using System.Threading.Tasks;
using Xunit;
using Moq;
using Pokemon.Controllers;

namespace PokemonTest.Controllers
{
    public class PokemonControllerTest
    {
        private readonly Mock<IPokemonService> _mockpokemonservice;
        private readonly PokemonController _controller;

        public PokemonControllerTest()
        {
            _mockpokemonservice = new Mock<IPokemonService>();
            _controller = new PokemonController(_mockpokemonservice.Object);
        }

        [Fact]
        public async Task GetPokemonStandardInfo_IfDeosntExist_NotFound()
        {
            _mockpokemonservice.Setup(srv => srv.GetPokemonStandardInfo(It.Is<string>(p => p == "hjkjhkhk"))).ReturnsAsync(new PokemonResource { });

            var result = await _controller.GetPokemonStandardInfo("hjkjhkhk");

            Assert.IsType<NotFoundResult>(result);
        }

        [Fact]
        public async Task GetPokemonStandardInfo_IfExists_ReturnPokemon()
        {
            _mockpokemonservice.Setup(srv => srv.GetPokemonStandardInfo(It.Is<string>(p => p == "hjkjhkhk"))).ReturnsAsync(new PokemonResource { name = "hjkjhkhk" });

            var result = await _controller.GetPokemonStandardInfo("hjkjhkhk");

            var actionResult = Assert.IsType<OkObjectResult>(result);

            var pokemonResource = Assert.IsType<PokemonResource>(actionResult.Value);

            Assert.Equal("hjkjhkhk", pokemonResource.name);
        }

        [Fact]
        public async Task GetPokemonTranslatedInfo_IfDeosntExist_NotFound()
        {
            _mockpokemonservice.Setup(srv => srv.GetPokemonTranslatedInfo(It.Is<string>(p => p == "hjkjhkhk"))).ReturnsAsync(new PokemonResource { });

            var result = await _controller.GetPokemonTranslatedInfo("hjkjhkhk");

            Assert.IsType<NotFoundResult>(result);
        }

        [Fact]
        public async Task GetPokemonTranslatedInfo_IfExists_ReturnPokemon()
        {
            _mockpokemonservice.Setup(srv => srv.GetPokemonTranslatedInfo(It.Is<string>(p => p == "hjkjhkhk"))).ReturnsAsync(new PokemonResource { name = "hjkjhkhk" });

            var result = await _controller.GetPokemonTranslatedInfo("hjkjhkhk");

            var actionResult = Assert.IsType<OkObjectResult>(result);

            var pokemonResource = Assert.IsType<PokemonResource>(actionResult.Value);

            Assert.Equal("hjkjhkhk", pokemonResource.name);
        }
    }
}
