﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Pokemon.Models;
using Pokemon.Services.Interfaces;
using System.Threading.Tasks;

namespace Pokemon.Controllers
{
    [Route("[controller]")]
    [ApiController]
    [ProducesResponseType(typeof(PokemonResource), StatusCodes.Status200OK)]
    [ProducesResponseType(typeof(NotFoundResult), StatusCodes.Status404NotFound)]
    public class PokemonController : ControllerBase
    {
        private readonly IPokemonService _pokemonService;

        public PokemonController(IPokemonService pokemonService)
        {
            _pokemonService = pokemonService;
        }

        [HttpGet("{name}")]
        public async Task<ActionResult> GetPokemonStandardInfo([FromRoute]string name)
        {
            var pokemon = await _pokemonService.GetPokemonStandardInfo(name);

            if (pokemon.name != null)
            {
                return Ok(pokemon);
            }
            else
            {
                return NotFound();
            }
        }

        [HttpGet("translated/{name}")]
        public async Task<ActionResult> GetPokemonTranslatedInfo([FromRoute]string name)
        {
            var pokemon = await _pokemonService.GetPokemonTranslatedInfo(name);

            if (pokemon.name != null)
            {
                return Ok(pokemon);
            }
            else
            {
                return NotFound();
            }
        }
    }
}
