﻿using Pokemon.Models;
using Pokemon.Repositories.Interfaces;
using Pokemon.Services.Interfaces;
using System;
using System.Threading.Tasks;

namespace Pokemon.Services
{
    public class PokemonService : IPokemonService
    {
        private readonly IPokemonRepository _pokemonRepo;
        private readonly ITranslationRepository _translationRepo;

        public PokemonService(IPokemonRepository pokemonRepo, ITranslationRepository translationRepo)
        {
            _pokemonRepo = pokemonRepo;
            _translationRepo = translationRepo;
        }

        public async Task<PokemonResource> GetPokemonStandardInfo(string name)
        {
            PokemonResource pokemon = await _pokemonRepo.GetPokemonFullInfo(name);

            if (pokemon.description != null)
            {
                pokemon.description = pokemon.description.Replace("\n", " ");

                pokemon.description = pokemon.description.Replace("\f", " ");
            }

            return pokemon;
        }

        public async Task<PokemonResource> GetPokemonTranslatedInfo(string name)
        {
            string url;

            PokemonResource pokemon = await _pokemonRepo.GetPokemonFullInfo(name);

            if (pokemon.name != null)
            {
                pokemon.description = pokemon.description.Replace("\n", " ");

                pokemon.description = pokemon.description.Replace("\f", " ");

                if (pokemon.habitat.ToLower() == "cave" || pokemon.isLegendary)
                {
                    url = string.Format("https://api.funtranslations.com/translate/yoda.json?text={0}", Uri.EscapeDataString(pokemon.description));
                }
                else
                {
                    url = string.Format("https://api.funtranslations.com/translate/shakespeare.json?text={0}", Uri.EscapeDataString(pokemon.description));
                }

                TranslationResource response = await _translationRepo.Translator(url);

                if (response.success.total > 0)
                {
                    pokemon.description = response.contents.translated;
                }
            }
           
            return pokemon;
        }
    }
}
