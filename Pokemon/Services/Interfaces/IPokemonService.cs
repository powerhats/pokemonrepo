﻿using Pokemon.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Pokemon.Services.Interfaces
{
    public interface IPokemonService
    {
        Task<PokemonResource> GetPokemonTranslatedInfo(string name);
        Task<PokemonResource> GetPokemonStandardInfo(string name);

    }
}
