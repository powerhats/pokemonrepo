﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Pokemon.Models
{
    public class PokemonResource
    {
        public string name { get; set; }
        public string description { get; set; }
        public string habitat { get; set; }
        public bool isLegendary { get; set; }
    }
}
