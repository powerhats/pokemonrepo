﻿using Pokemon.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Pokemon.Repositories.Interfaces
{
    public interface ITranslationRepository
    {
        Task<TranslationResource> Translator(string url);
    }
}
