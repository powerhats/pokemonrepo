﻿using Pokemon.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Pokemon.Repositories.Interfaces
{
    public interface IPokemonRepository
    {
        Task<PokemonResource> GetPokemonFullInfo(string name);
    }
}
