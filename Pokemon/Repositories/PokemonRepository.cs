﻿using Newtonsoft.Json;
using Pokemon.Models;
using Pokemon.Repositories.Interfaces;
using System.Net.Http;
using System.Threading.Tasks;

namespace Pokemon.Repositories
{
    public class PokemonRepository : IPokemonRepository
    {
        public async Task<PokemonResource> GetPokemonFullInfo(string name)
        {
            PokemonResource pokemonresult = new PokemonResource { };

            using (var httpClient = new HttpClient())
            {
                using (var result = await httpClient.GetAsync(string.Format("https://pokeapi.co/api/v2/pokemon-species/{0}", name)))
                {
                    if ((int)result.StatusCode == 200)
                    {
                        string stringresult = await result.Content.ReadAsStringAsync();

                        dynamic objectresult = JsonConvert.DeserializeObject(stringresult);

                        foreach (var item in objectresult.flavor_text_entries)
                        {
                            if (item.language.name == "en")
                            {
                                pokemonresult.description = item.flavor_text;
                                break;
                            }
                        }

                        pokemonresult.name = objectresult.name;

                        pokemonresult.habitat = objectresult.habitat.name;

                        pokemonresult.isLegendary = objectresult.is_legendary;
                    }
                }
            }

            return pokemonresult;
        }
    }
}
