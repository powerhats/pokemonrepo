﻿using Newtonsoft.Json;
using Pokemon.Models;
using Pokemon.Repositories.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;

namespace Pokemon.Repositories
{
    public class TranslationRepository : ITranslationRepository
    {
        public async Task<TranslationResource> Translator(string url)
        {
            TranslationResource translationresult = new TranslationResource { success = new Success { }, contents = new Contents { } };

            using (var httpClient = new HttpClient())
            {
                using (var result = await httpClient.GetAsync(url))
                {
                    if ((int)result.StatusCode == 200)
                    {
                        string stringresult = await result.Content.ReadAsStringAsync();

                        translationresult = JsonConvert.DeserializeObject<TranslationResource>(stringresult);
                    }
                }
            }

            return translationresult;
        }
    }
}
