### Steps to run project: ###

* 1. Download/Clone the repository
* 2. Open the soltuion (Pokemon.sln) in Visual Studio and build it
* 3. Run application
* 4. Alternatively, run the executable in Pokemon\Pokemon\bin\Debug\netcoreapp3.1 (Pokemon.exe) and open the browser in the web address provided in the popup console


### How it works: ###

* 1. By running the app, a browser window will open
* 2. Put a / following the name or ID of the pokemon after the address and hit enter to call the normal pokemon info endpoint
* 3. Put /translated/ following the name or ID of the pokemon after the address and hit enter to call the translating endpoint


### What I would have done differently in production: ###

* The data access layer (repository) would call a databse, so ideally I wouldn't use an HttpClient there.